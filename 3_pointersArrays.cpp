#include <iostream>
using namespace std;

int main()
{
    /*
    int myArrays[5] = {8, 18, 188, 1888, 18888};
    //第一个元素的地址
    cout << myArrays << endl;
    cout << &myArrays[0] << endl;

    //数组索引
    cout << myArrays[2] << endl;//output: 188
    cout << *(myArrays + 2) << endl;//output: 188
    */
    int myArrays[5];

    for (int i = 0; i <= 4; i++)
    {
        cout << "Number: ";
        cin >> myArrays[i];
    }

    for (int i = 0; i <= 4; i++)
    {
        cout << myArrays[i] <<" ";
        cout << *(myArrays + i) <<" ";
    }

    system("pause > 0");
    return 0;
}