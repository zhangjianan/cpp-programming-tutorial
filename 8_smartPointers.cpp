#include <iostream>
using namespace std;
#include <memory>

class myClass
{
public:
    myClass(){
        cout << "Start - 构造函数被调用..." << endl;
    } 
    ~myClass(){
        cout << "End - 析构函数被调用..." << endl;
    }   
};


int main()
{
    //unique_ptr<int>unPtr1  = make_unique<int>(25);
    //cout << unPtr1 << endl;
    //cout << *unPtr1 << endl;
    //unique_ptr<int>unPtr2  = move(unPtr1);
    
    // {
    // unique_ptr<myClass>unPtr1 = make_unique<myClass>();
    // }
    {
        shared_ptr<myClass>shPtr1 = make_shared<myClass>();
        cout << "shared count: " << shPtr1.use_count() << endl;
        {
            shared_ptr<myClass>shPtr2 = shPtr1;
            cout << "shared count: " << shPtr1.use_count() << endl;
        }
        cout << "shared count: " << shPtr1.use_count() << endl;
    }


    system("pause > 0");
    return 0;
}