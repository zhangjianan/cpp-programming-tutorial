#include <iostream>
using namespace std;

//动态数组、动态内存分配
int main()
{
    int size;
    cout << "Size: ";
    cin >> size;
    //int myArray[size];//编译器不知道数组的大小，则会报错

    //动态分配
    int* myArray = new int[size];
    for(int i = 0; i < size; i++)//获取用户输入大小的动态数组
    {
        cout << "Array[" << i << "] ";
        cin >> myArray[i];
    }

    for(int i = 0; i < size; i++)//打印用户输入大小的动态数组
    {
        //cout << myArray[i] << "  ";
        cout << *(myArray + i) << "  ";
    }

    //动态分配的内存使用后记得删除
    delete[]myArray;
    myArray = NULL;

    system("pause > 0");
    return 0;
}