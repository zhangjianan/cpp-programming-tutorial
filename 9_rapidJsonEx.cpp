//C++ – 如何使用 RapidJSON 库处理 JSON
//一个快速的 C++ JSON 解析和生成库。

//解析 JSON 字符串

#include <iostream>

#include "rapidjson/document.h"

using namespace rapidjson;

int main(int, char **)
{
    std::string json_str = "{\"hello\": \"world\", "
                           "\"t\": true, "
                           "\"f\": false, "
                           "\"n\": null, "
                           "\"i\": 123, "
                           "\"pi\": 3.1416, "
                           "\"a\": [1, 2, 3, 4]}";

    Document doc;
    doc.Parse(json_str.c_str());

}