#include <iostream>
#include "include/document.h"

#include <fstream>
#include "include/istreamwrapper.h"

#include "include/stringbuffer.h"
#include "include/writer.h"

#include "include/ostreamwrapper.h"

using namespace rapidjson;


//-----------------------------------------
//------------获取 JSON 值----------------
//-----------------------------------------
int main(int, char **)
{
    // std::string json_str = "{\"hello\": \"world\", "
    //                        "\"true\": true, "
    //                        "\"false\": false, "
    //                        "\"null\": null, "
    //                        "\"int\": -123, "
    //                        "\"uint\": 123, "
    //                        "\"int64\": -870744036720833075, "
    //                        "\"uint64\": 870744036720833075, "
    //                        "\"double\": 3.1416, "
    //                        "\"object\": {\"hoge\": 1}, "
    //                        "\"array\": [1, 2, 3, 4]}";

    // Document doc;
    // doc.Parse(json_str.c_str());

    // 1.类型判断：使用以 Is 开头的方法
    /*
    std::cout << "IsString: " << doc["hello"].IsString() << std::endl; // 是否为字符串
    std::cout << "IsNull: " << doc["null"].IsNull() << std::endl;      // 是否为空
    std::cout << "IsFalse: " << doc["false"].IsFalse() << std::endl;   // 是否为false
    std::cout << "IsTrue: " << doc["true"].IsTrue() << std::endl;      // 是否为true
    std::cout << "IsBool: " << doc["true"].IsBool() << std::endl;      // 是否为布尔值
    std::cout << "IsObject: " << doc["object"].IsObject() << std::endl; // 是否为对象
    std::cout << "IsArray: " << doc["array"].IsArray() << std::endl; // 是否为数组
    std::cout << "IsNumber: " << doc["int"].IsNumber() << std::endl; // 是否为数字
    std::cout << "IsInt: " << doc["int"].IsInt() << std::endl; // 是否为int
    std::cout << "IsUint: " << doc["uint"].IsUint() << std::endl; // 是否为uint
    std::cout << "IsInt64: " << doc["int64"].IsInt64() << std::endl; // 是否为int64
    std::cout << "IsUint64: " << doc["uint64"].IsUint64() << std::endl; // 是否为uint64
    std::cout << "IsDouble: " << doc["double"].IsDouble() << std::endl; // 是否为double
    */
    
    // 2.获取值：使用以 Get 开头的方法
    /*
    std::cout << "--------------------------" << std::endl;
    std::cout << "GetBool: " << doc["true"].GetBool() << std::endl; // 真假值
    std::cout << "GetString: " << doc["hello"].GetString() << std::endl; // 字符串
    std::cout << "GetInt: " << doc["int"].GetInt() << std::endl; // 数値
    std::cout << "GetUint: " << doc["uint"].GetUint() << std::endl;
    std::cout << "GetInt64: " << doc["int64"].GetInt64() << std::endl;
    std::cout << "GetUint64: " << doc["uint64"].GetUint64() << std::endl;
    std::cout << "GetDouble: " << doc["double"].GetDouble() << std::endl;
    std::cout << "Object: " << doc["object"]["hoge"].IsInt() << std::endl; // 对象
    
    for (auto itr = doc["array"].Begin(); itr != doc["array"].End(); ++itr) // 数组
        std::cout << itr->GetInt() << " ";
    std::cout << std::endl;

    for (size_t i = 0; i < doc["array"].Size(); ++i)
        std::cout << doc["array"][i].GetInt() << " ";
    std::cout << std::endl;
    */

    // 3.值设定：修改现有值（布尔值、数字、字符串）
    /*
    std::cout << "--------------------------" << std::endl;
    doc["true"].SetBool(false); // 真假值
    doc["hello"].SetString("Chinese"); // 字符串
    doc["int"].SetInt(-7);     // 数値
    doc["uint"].SetUint(7);
    doc["int64"].SetInt64(-570744036720833075);
    doc["uint64"].SetUint64(570744036720833075);
    doc["double"].SetDouble(1.57);

    
    // StringBuffer buffer;
    // Writer<StringBuffer> writer(buffer);
    // doc.Accept(writer);
    // std::cout << buffer.GetString() << std::endl;


    std::ofstream ofs("output1.json");
    OStreamWrapper osw(ofs);

    Writer<OStreamWrapper> writer(osw);
    doc.Accept(writer);
    */
    
    

    // 4.编辑对象：可以使用 AddMember() 将子 Value 对象添加到对象类型 Value 对象中
    /*
    std::string json_str1 = "{\"hello\": \"world\"}";

    Document doc;
    doc.Parse(json_str1.c_str());

    doc.AddMember("pi", 3.14, doc.GetAllocator()); // 为对象增加值

    Value child(kObjectType); // 创建一个新的对象类型值
    child.AddMember("hoge", 1, doc.GetAllocator());
    child.AddMember("fuga", 2, doc.GetAllocator());
    doc.AddMember("child", child, doc.GetAllocator());

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
    // {"hello":"world","pi":3.14,"child":{"hoge":1,"fuga":2}}
    */

    // 5.编辑数组：可以使用 PushBack() 将值添加到数组类型的 Value 对象中
    /*
    std::string json_str = "{\"array\": [1, 2]}";

    Document doc;
    doc.Parse(json_str.c_str());

    // 为对象增加值
    doc["array"].PushBack(3, doc.GetAllocator());
    doc["array"].PushBack(4, doc.GetAllocator());
    doc["array"].PushBack(5, doc.GetAllocator());

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
    // {"array":[1,2,3,4,5]}
    */

    // 6.Json输出
    // 对于 JSON 输出，创建一个指定输出目标的 Writer 对象，并使用 Document.Accept() 将其绑定到 Document 对象。
    // 当 buffer.GetString() 需要 JSON 输出时，输出实际上是使用关联的 Writer 对象生成的

    // 6.1.输出为字符串：以字符串形式输出时，将 StringBuffer 指定为 Writer 的输出目的地
    /*
    Document doc(kObjectType);
    doc.AddMember("pi", 3.14, doc.GetAllocator());
    doc.AddMember("hello", "world", doc.GetAllocator());

    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
    // {"pi":3.14,"hello":"world"}
    */


    // 6.2.输出到文件：输出到文件时，将 OStreamWrapper 指定为 Writer 的输出目标
    /*
    Document doc(kObjectType);
    doc.AddMember("pi", 3.14, doc.GetAllocator());
    doc.AddMember("hello", "world", doc.GetAllocator());

    std::ofstream ofs("output.json");
    OStreamWrapper osw(ofs);

    Writer<OStreamWrapper> writer(osw);
    doc.Accept(writer);
    */

    // 7.Json输入：解析 Json 文件
    
    std::ifstream ifs("groundTruth.json");
    IStreamWrapper isw(ifs);

    Document doc;
    doc.ParseStream(isw);

    //std::cout << doc["TempList"]["zuohengliang"]["img"].GetString() << std::endl;
    
    StringBuffer buffer;
    Writer<StringBuffer> writer(buffer);
    doc.Accept(writer);
    std::cout << buffer.GetString() << std::endl;
    
}
