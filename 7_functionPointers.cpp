// #include <iostream>
// using namespace std;

// int getNumber()
// {
//     return 5;
// }

// int add(int a, int b)
// {
//     return a + b;
// }

// int main()
// {
//     cout << getNumber() << endl;//output:5
//     cout << getNumber << endl;//output:010011E0
    
//     //创建函数指针
//     int(*funcPtr)() = getNumber;
//     cout << funcPtr() << endl;

//     //创建带有参数的函数指针
//     int(*addFuncPtr)(int, int) = add;
//     cout << add(12, 23) << endl;
//     cout << addFuncPtr(11, 45) << endl;


//     system("pause > 0");
//     return 0;
// }

/*+++++++++++++++++++++++++++以上未能看错函数指针的意义++++++++++++++++++++++++*/
/*+++++++++++++++++++++++++++以下进行演示++++++++++++++++++++++++*/

#include <iostream>
#include <vector>
using namespace std;

bool ascendingCompare(int a, int b)
{
    return a < b;
}

bool descendingCompare(int a, int b)
{
    return a > b;
}

// //升序算法函数
// void sortAscending(vector<int>& numbersVector)
// {
//     for(int startIndex = 0; startIndex < numbersVector.size(); startIndex++)
//     {
//         int bestIndex = startIndex;
//         for(int currentIndex = startIndex + 1; currentIndex < numbersVector.size(); currentIndex++)
//         {
//             //在此做比较
//             if(ascendingCompare(numbersVector[currentIndex], numbersVector[bestIndex]))
//                 bestIndex = currentIndex;
//         }
//         swap(numbersVector[startIndex], numbersVector[bestIndex]);
//     }
// }

// //降序算法函数
// void sortDescending(vector<int>& numbersVector)
// {
//     for(int startIndex = 0; startIndex < numbersVector.size(); startIndex++)
//     {
//         int bestIndex = startIndex;
//         for(int currentIndex = startIndex + 1; currentIndex < numbersVector.size(); currentIndex++)
//         {
//             //在此做比较
//             if(descendingCompare(numbersVector[currentIndex], numbersVector[bestIndex]))
//                 bestIndex = currentIndex;
//         }
//         swap(numbersVector[startIndex], numbersVector[bestIndex]);
//     }
// }

void customSort(vector<int>& numbersVector, bool(*compareFuncPtr)(int, int))
{
    for(int startIndex = 0; startIndex < numbersVector.size(); startIndex++)
    {
        int bestIndex = startIndex;
        for(int currentIndex = startIndex + 1; currentIndex < numbersVector.size(); currentIndex++)
        {
            //在此做比较
            if(compareFuncPtr(numbersVector[currentIndex], numbersVector[bestIndex]))
                bestIndex = currentIndex;
        }
        swap(numbersVector[startIndex], numbersVector[bestIndex]);
    }
}

//打印排序后的数字
void printNumbers(vector<int>& numbersVector)
{
    for(int i = 0; i < numbersVector.size(); ++i)
    {
        cout << numbersVector[i] << ' ';
    }
}

int main()
{
    vector<int> myNumbers = {12, 188, 255, 433, 766, 325};
    // sortAscending(myNumbers);
    // printNumbers(myNumbers);

    // sortDescending(myNumbers);
    // printNumbers(myNumbers);

    bool(*funcPtr)(int, int) = descendingCompare;
    customSort(myNumbers, funcPtr);
    printNumbers(myNumbers);


    system("pause > 0");
    return 0;
}

