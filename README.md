# cpp-programming-tutorial

> 内容:

1. C++ 指针
2. void 指针
3. 指针与数组
4. 使用指针从函数返回多个值
5. 动态数组 - 如何在运行时创建/更改数组
6. 多维动态数组（二维动态数组）
7. 如何检测代码中的错误和错误? Visual Studio/扩展 PVS-Studio
8. 解释 C++ 中的内存泄漏
9. 函数指针
10. 智能指针(unique, shared, weak)
