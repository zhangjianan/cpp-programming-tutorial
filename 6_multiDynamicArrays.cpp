#include <iostream>
using namespace std;

int main()
{
    int rows, cols;
    cout << "rows, cols:";
    cin >> rows >> cols;

    int** table = new int*[rows];
    for(int i = 0; i < rows; i++)
    {
        table[i] = new int[cols];
    }

    table[1][2] = 88;

    //清内存
    for(int i = 0; i < rows; i++)
    {
        delete[] table[i];//倒数第1层
    }
    delete[] table;//倒数第2层
    table = NULL;//倒数第3层

    system("pause > 0");
    return 0;
}