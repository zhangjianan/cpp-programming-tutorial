#include <iostream>
using namespace std;

void printNumber(int* numberPtr)
{
    cout << "*numberPtr: " << *numberPtr << endl;
}

void printLetter(char* charPtr)
{
    cout << "*charPtr: " << *charPtr << endl;
}

//void 指针可以指向任何类型的变量，但是不能直接解引用(*ptr)
void print(void* ptr,char type)
{
    switch (type)
    {
    case 'i':
        cout << *((int*)ptr) << endl;
        break;
    case 'c':
        cout << *((char*)ptr) << endl;
        break;
    }
}

int main()
{
    int number = 5;
    printNumber(&number);

    char letter = 'a';
    printLetter(&letter);

    //使用void ptr打印
    print(&number, 'i');
    print(&letter, 'c');

    system("pause > 0");
    return 0;
}